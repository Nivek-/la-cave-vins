<?php
require_once("autoload.php");

// Ajout dynamique d'un produit au panier via une requête AJAX
if(isset($_POST["ProductId"])) {
    $user = new Users();
    $user = $_SESSION["user"];
    $cart = new Cart($user->getId());
    $cart->setProductId($_POST["ProductId"]);
    $cart->setQuantity($_POST["Quantity"]);
    $cart->addCart();
    echo json_encode(array('success'=>$cart));
}
?>