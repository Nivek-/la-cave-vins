<article class="introduction">
    <div class="intro_img"></div>
    <h1>Une sélection de vins pour satisfaire tous les palais</h1>
</article>

<article class="container">
    <div class="selection">
        <h2>Notre sélection du moment</h2>
        <div class="selection_blocs">
        <?php
// Affichage de 3 produits sélectionnés aléatoirement dans la base de données
$BDD = new BDD();
$dbh = $BDD->getConnection();
$sth = $dbh->prepare('SELECT * from `product` ORDER BY RAND() LIMIT 3');
$results = $sth->execute(array());
while ($data = $sth->fetch(PDO::FETCH_OBJ)) {
    echo "<div class='products_bloc'>";
    echo "<a href='index.php?page=detail&product=" . $data->product_id . "' class='product_card'>";
    echo "<img src='assets/images/" . $data->image . "' alt='Bouteille de " . $data->name . " " . $data->year . "' class='wine_bottle' />";
    echo "<h4>" . $data->name . " " . $data->year . "</h4>";
    echo "<p>" . $data->price . " € TTC</p>";
    echo "</a>";
    echo "<div class='cart_status'>";
    if ($data->stock > 0) {
        echo "<div class='stock'>";
        echo "<i class='fas fa-check-circle'></i>";
        echo "<p>En stock</p>";
        echo "</div>";
        echo "<a class='addcart_button add-cart'>ajouter au panier</a>";

    } else {
        echo "<div class='stock'>";
        echo "<i class='fas fa-times-circle'></i>";
        echo "<p>Rupture de stock</p>";
        echo "</div>";
        echo "<a class='addcart_button unclick'>ajouter au panier</a>";
    }
    echo "</div>";
    echo "</div>";
}

?>
        </div>
    </div>

    <!-- Boutons pour accéder aux différentes catégories  -->

    <div class="wine_categories">
        <div class="two_categories">
            <a href="index.php?page=products&category=Vins%20de%20France" class="category_content">
                <img src="assets/images/france.jpg" class="categories_img" alt="Tonneaux de vin" />
                <h3>Vins de france</h3>
            </a>
            <a href="index.php?page=products&category=Vins%20du%20monde" class="category_content">
                <img src="assets/images/world.jpg" class="categories_img"
                    alt="Châteaux entouré de vignes en Allemagne" />
                <h3>Vins du monde</h3>
            </a>
        </div>
        <div class="two_categories">
            <a href="index.php?page=products&category=champagne" class="category_content">
                <img src="assets/images/champagne.jpg" class="categories_img"
                    alt="Plateau avec des flûtes de champagne" />
                <h3>Champagne</h3>
            </a>
            <a href="index.php?page=products&category=Tous%20nos%20produits" class="category_content">
                <img src="assets/images/color.jpg" class="categories_img"
                    alt="Verres contenant des vins de robes différentes" />
                <h3>Tous nos produits</h3>
            </a>
        </div>
    </div>

    <!-- Popin visible lorsque l'utilisateur clique sur ajouter au panier -->
    <div class="modal fade" id="modalProduct" tabindex="-1" role="dialog" aria-labelledby="modalProductLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="modalProductLabel"></h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="row">
            <div class="col-4"></div>
            <div class="col-8">
              <div class="produit"></div>
              <div class="actions">
                <label for="quantity">Quantité :</label>
                <input type="number" min="1" name="quantity" id="quantity">
                <input type="hidden" id="product_id">
                <button class="btn-add-to-card">Valider l'ajout</button>
              </div>
              <p id='product_added'>Le produit a bien été ajouté au panier</p>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Annuler</button>
          </div>
        </div>
      </div>
    </div>
  </div>
</article>