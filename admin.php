<?php
$admin = new Users();
$admin = $_SESSION["user"];
if (!isset($_SESSION["user"]) || (isset($_SESSION["user"]) && $admin->getAdmin() != 1)) {
    header("Location: index.php?page=home");
    exit();
}
?>

<article class="container">
    <div class="admin_content">
        <h1>Administration</h1>
        <div class="admin_categories">
            <a href="index.php?page=change_products" class="card_category">
            <i class="fas fa-wine-glass"></i>
                <h3>Ajouter / Supprimer un produit</h3>
            </a>
            <a href="index.php?page=change_rights" class="card_category">
            <i class="fas fa-users-cog"></i>
                <h3>Changer les droits d'administration</h3>
            </a>
        </div>
    </div>
</article>
