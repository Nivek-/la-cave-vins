<article class="container">
  <div class="products_page">
    <h1>Les produits</h1>
    <div class="product_content">
      <div class="products_categories">
        <?php
// Création d'un menu latéral avec les catégories, les origines et les couleurs
createNav();
?>
      </div>
      <div class="products">

        <?php
// Affichage des produits en fontion des catégories, origines ou couleurs
// La récupération se fait via un GET

if (isset($_GET["category"])) {
    $cat = $_GET["category"];
    $cat_str = str_replace("%20", " ", $cat);
    if ($cat_str == "Tous nos produits") {
        showAllProducts();
    } else {
        $find_cat = new Category($cat_str);
        $find_cat->searchCategory();
    }
} else if (isset($_GET["origin"])) {
    $ori = $_GET["origin"];
    $ori_str = str_replace("%20", " ", $ori);
    $find_ori = new Origin($ori_str);
    $find_ori->searchOrigin();

} else if (isset($_GET["color"])) {
    $col = $_GET["color"];
    $col_str = str_replace("%20", " ", $col);
    $find_col = new Color($col_str);
    $find_col->searchColor();
}

?>
      </div>
    </div>
  </div>

  <!-- Popin visible lorsque l'utilisateur clique sur ajouter au panier -->

  <div class="modal fade" id="modalProduct" tabindex="-1" role="dialog" aria-labelledby="modalProductLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="modalProductLabel"></h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="row">
            <div class="col-4"></div>
            <div class="col-8">
              <div class="produit"></div>
              <div class="actions">
                <label for="quantity">Quantité :</label>
                <input type="number" min="1" name="quantity" id="quantity">
                <input type="hidden" id="product_id">
                <button class="btn-add-to-card">Valider l'ajout</button>
              </div>
              <p id='product_added'>Le produit a bien été ajouté au panier</p>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Annuler</button>
          </div>
        </div>
      </div>
    </div>
  </div>
</article>