<?php

// Récupération de l'id du produit via GET
// Récupération des informations du produit dans la BDD grâce à son id
$BDD = new BDD();
$dbh = $BDD->getConnection();
$result = $dbh->prepare('SELECT * from `product` where `product_id` = ?');
$result->execute(array($_GET["product"]));
foreach ($result as $row) {
    $product = new Product();
    $product->setCategory($row["category"]);
    $product->setName($row["name"]);
    $product->setYear($row["year"]);
    $product->setPrice($row["price"]);
    $product->setDescribe($row["describe"]);
    $product->setImage($row["image"]);
    $product->setStock($row["stock"]);
    $product->setColor($row["color"]);
    $product->setOrigin($row["origin"]);
    $product->setId($row["product_id"]);
}

?>


<article class="container">
    <div class="detail_content">
        <div class="first_datas">
            <?php

            // Affichage des informations du produit

echo "<img src='assets/images/" . $product->getImage() . "' alt='Bouteille de " . $product->getName() . " " . $product->getYear() . "' class='wine_bottle' />";
echo "<div class='presentation'>";
echo "<h2>" . $product->getName() . " " . $product->getYear() . "</h2>";
echo "<h3>" . $product->getPrice() . " € TT</h3>";
echo "<p>".$product->getDescribe()."</p>";
echo "<div class='stock'>";
if ($product->getStock() != 0) {
    echo "<i class='fas fa-check-circle'></i>";
    echo "<p>En stock</p>";
    echo "</div>";
    echo "<a class='addcart_button add-cart'>ajouter au panier</a>";
} else {
    echo "<i class='fas fa-times-circle'></i>";
    echo "<p>Rupture de stock</p>";
    echo "</div>";
    echo "<a class='addcart_button unclick'>ajouter au panier</a>";
}

echo "</div>";
?>
        </div>
        <div class="detail_datas">
            <?php

if ($product->getCategory() != "Champagne") {
    echo "<div class='detail_category'>";
    echo "<p class='bold'>Origine</p>";
    echo "<p>" . $product->getOrigin() . "</p>";
    echo "</div>";
}

echo "<div class='detail_category'>";
echo "<p class='bold'>Appellation</p>";
echo "<p>" . $product->getName() . "</p>";
echo "</div>";

if ($product->getCategory() != "Champagne") {
    echo "<div class='detail_category'>";
    echo "<p class='bold'>Robe</p>";
    if ($product->getColor() == "Vins rouges") {
        echo "<p>Rouge</p>";
    } else if($product->getColor() == "Vins blancs") {
        echo "<p>Blanc</p>";
    } else {
        echo "<p>Rosé</p>";
    }
    echo "</div>";
}

echo "<div class='detail_category'>";
echo "<p class='bold'>Millésime</p>";
echo "<p>" . $product->getYear()."</p>";
echo "</div>";

echo "<div class='detail_category'>";
echo "<p class='bold'>Contenance</p>";
echo "<p>75 cl</p>";
echo "</div>";
?>
        </div>
    </div>

    <!-- Popin visible lorsque l'utilisateur clique sur ajouter au panier -->
    <div class="modal fade" id="modalProduct" tabindex="-1" role="dialog" aria-labelledby="modalProductLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="modalProductLabel"></h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="row">
            <div class="col-4"></div>
            <div class="col-8">
              <div class="produit"></div>
              <div class="actions">
                <label for="quantity">Quantité :</label>
                <input type="number" min="1" name="quantity" id="quantity">
                <input type="hidden" id="product_id">
                <button class="btn-add-to-card">Valider l'ajout</button>
              </div>
              <p id='product_added'>Le produit a bien été ajouté au panier</p>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Annuler</button>
          </div>
        </div>
      </div>
    </div>
  </div>
</article>