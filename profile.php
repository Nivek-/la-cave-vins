<?php
if (!isset($_SESSION["user"])) {
    // Redirection vers la page login si l'utilisateur n'est pas connecté
    header("Location: index.php?page=login");
    exit();
}
?>

<div class="container">
    <div class="profile_content">
        <h1>Vos informations personnelles</h1>
        <?php
$logged = new Users();
$logged = $_SESSION["user"];

// Changement des informations de l'utilisateur après plusieurs vérifications faites sur les champs saisis et la BDD
// Un message d'erreur en fonction de la condition non respectée
// S'il n'y a pas d'erreur le changement se fait en BDD
if (isset($_POST["submit_btn"])) {
    if ($_POST["submit_btn"] == "Enregistrer") {
        if (empty($_POST["firstname"]) || empty($_POST["lastname"])) {
            echo "<p>Il faut remplir tous les champs avec une *</p>";
        } else {
            if (empty($_POST["password"]) && !empty($_POST["new_password"])) {
                echo "<p>Veuillez entrer votre mot de passe actuel pour le changer</p>";
            } else if (!empty($_POST["password"]) && !empty($_POST["new_password"])) {
                if ($_POST["password"] != $logged->getPassword()) {
                    echo "<p>Mot de passe erroné</p>";
                } else {
                    // Changement avec mot de passe compris
                    $logged->changeWithPassword();
                }
            } else if (empty($_POST["password"]) && empty($_POST["new_password"])) {
                // Changement sans mot de passe
                $logged->changeExceptPass();
            }
        }
    }
}

// Message de confirmation si les changements ont été pris en compte
if(isset($_GET["change"]) && $_GET["change"] == "valid") {
    echo "<p>Modifications enregistrées</p>";
}

?>
        <!-- Formulaire de modification pré-rempli avec les informations de l'utilisateur importées depuis la BDD -->
        
        <div class="form_bloc">
                <form method="POST" action="index.php?page=profile">
                    <div class="form_cat">
                        <label for="lastname">Nom*</label>
                        <?php
echo "<input type='text' name='lastname' id='lastname' value='" . $logged->getLastname() . "' />";
?>
                    </div>
                    <div class="form_cat">
                        <label for="firstname">Prénom*</label>
                        <?php
echo "<input type='text' name='firstname' id='firstname' value='" . $logged->getFirstname() . "' />";
?>
                    </div>
                    <div class="form_cat">
                        <label for="birthdate">Date de naissance (optionnel)</label>
                        <?php
echo "<input type='date' name='birthdate' id='birhtdate' value='" . $logged->getBirthdate() . "' />";
?>
                    </div>
                    <div class="form_cat">
                        <label for="password">Mot de passe</label>
                        <input type="password" name="password" id="password" />
                    </div>
                    <div class="form_cat">
                        <label for="new_password">Nouveau mot de passe (optionnel)</label>
                        <input type="password" name="new_password" id="new_password" />
                    </div>
                    <div class="form_cat">
                        <label for="number">Numéro de téléphone (optionnel)</label>
                        <?php
echo "<input type='tel' name='number' id='number' value='" . $logged->getPhonenumber() . "' />";
?>
                    </div>
                    <input type="submit" value="Enregistrer" name="submit_btn" class="valid_btn">
                </form>
        </div>
    </div>
</div>