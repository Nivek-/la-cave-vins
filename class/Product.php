<?php
class Product
{
    private $id;
    private $category;
    private $name;
    private $year;
    private $price;
    private $describe;
    private $color;
    private $origin;
    private $image;
    private $stock;

    // Getter et setter
    public function setCategory($category) {
        $this->category = $category;
    }

    public function getCategory()
    {
        return $this->category;
    }

    public function setName($name) {
        $this->name = $name;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setYear($year) {
        $this->year = $year;
    }

    public function getYear()
    {
        return $this->year;
    }

    public function setPrice($price) {
        $this->price = $price;
    }

    public function getPrice()
    {
        return $this->price;
    }

    public function setDescribe($describe) {
        $this->describe = $describe;
    }

    public function getDescribe()
    {
        return $this->describe;
    }

    public function setImage($image) {
        $this->image = $image;
    }

    public function getImage()
    {
        return $this->image;
    }

    public function setStock($stock) {
        $this->stock = $stock;
    }

    public function getStock()
    {
        return $this->stock;
    }

    public function setColor($color) {
        $this->color = $color;
    }

    public function getColor()
    {
        return $this->color;
    }

    public function setOrigin($origin) {
        $this->origin = $origin;
    }

    public function getOrigin() {
        return $this->origin;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function getId() {
        return $this->id;
    }
    // Fin des getter et setter


    // Ajout d'un nouveau produit dans la table Product
    public function save()
    {
        $BDD = new BDD();
        $dbh = $BDD->getConnection();
        $cat = $this->getCategory();
        $n = $this->getName();
        $y = $this->getYear();
        $p = $this->getPrice();
        $d = $this->getDescribe();
        $c = $this->getColor();
        $o = $this->getOrigin();
        $i = $this->getImage();
        $s = $this->getStock();
        $sth = $dbh->prepare('INSERT INTO `product` (`category`, `name`,`year`,`price`,`describe`, `color`,`origin`,`image`, `stock`) VALUES (:cat,:n, :y , :price ,:describe , :color,:origin, :img, :stock)');
        $sth->bindParam(':cat', $cat);
        $sth->bindParam(':n', $n);
        $sth->bindParam(':y', $y);
        $sth->bindParam(':price', $p);
        $sth->bindParam(':describe', $d);
        $sth->bindParam(':color', $c);
        $sth->bindParam(':origin', $o);
        $sth->bindParam(':img', $i);
        $sth->bindParam(':stock', $s);
        $sth->execute();
    }

    // Affichage d'un article
    public function showArticle($product) 
    {
        $BDD = new BDD();
        $dbh = $BDD->getConnection();
        $sth = $dbh->prepare('SELECT * FROM `product` WHERE `product_id` = ?');
        $result = $sth->execute(array($product));
        $data = $sth->fetch(PDO::FETCH_OBJ);
        $this->setImage($data->image);
        $this->setName($data->name);
        $this->setPrice($data->price);
        $this->setYear($data->year);
        $this->setId($product);
        $this->setStock($data->stock);
    }
}
?>