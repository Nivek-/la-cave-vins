<?php
class Color
{
    public $id;
    public $name;

    // Constructeur de la classe Color
    public function __construct($name)
    {
        $this->name = $name;
    }

    // Affichage de tous les produits d'une couleur de robe
    public function searchColor()
    {
        $BDD = new BDD();
        $dbh = $BDD->getConnection();
        $sth = $dbh->prepare('SELECT  `product_id`, `name`,`year`,`price`,`describe`, `color`,`origin`,`image`, `stock` from `product` WHERE `color` = ?');
        $results = $sth->execute(array($this->name));
        while ($data = $sth->fetch(PDO::FETCH_OBJ)) {
            echo "<div class='products_bloc' id='".$data->product_id."'>";
            echo "<a href='index.php?page=detail&product=" . $data->product_id . "' class='product_card'>";
            echo "<img src='assets/images/" . $data->image . "' alt='Bouteille de " . $data->name . " " . $data->year . "' class='wine_bottle' />";
            echo "<h4>" . $data->name . " " . $data->year . "</h4>";
            echo "<p>" . $data->price . " € TTC</p>";
            echo "</a>";
            echo "<div class='cart_status'>";
            if ($data->stock > 0) {
                echo "<div class='stock'>";
                echo "<i class='fas fa-check-circle'></i>";
                echo "<p>En stock (<span class='stock_value'>".$data->stock."</span>)</p>";
                echo "</div>";
                echo "<a class='addcart_button add-cart'>ajouter au panier</a>";

            } else {
                echo "<div class='stock'>";
                echo "<i class='fas fa-times-circle'></i>";
                echo "<p>Rupture de stock</p>";
                echo "</div>";
                echo "<a class='addcart_button unclick'>ajouter au panier</a>";
            }
            echo "</div>";
            echo "</div>";
        }
    }
}
?>