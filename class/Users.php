<?php
class Users
{
    private $mail;
    private $id;
    private $lastname;
    private $firstname;
    private $password;
    private $birthdate;
    private $phonenumber;
    private $admin;

    //Getter et Setter de tous les paramètres -> ligne 100

    public function setMail($mail)
    {
        $this->mail = $mail;
    }

    public function getMail()
    {
        return $this->mail;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setLastname($lastname)
    {
        $this->lastname = $lastname;
    }

    public function getLastname()
    {
        return $this->lastname;
    }

    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;
    }

    public function getFirstname()
    {
        return $this->firstname;
    }

    public function setPassword($password)
    {
        $this->password = $password;
    }

    public function getPassword()
    {
        return $this->password;
    }

    public function setBirthdate($birthdate)
    {
        $this->birthdate = $birthdate;
    }

    public function getBirthdate()
    {
        return $this->birthdate;
    }

    public function setPhoneNumber($phonenumber)
    {
        $this->phonenumber = $phonenumber;
    }

    public function getPhonenumber()
    {
        return $this->phonenumber;
    }

    public function setAdmin($admin)
    {
        $this->admin = $admin;
    }

    public function getAdmin()
    {
        return $this->admin;
    }

// Fin des Getter et Setter

    // Connexion de l'utilisateur avec vérification de l'adresse mail et du mot de passe dans la base de données
    public function login()
    {
        $BDD = new BDD();
        $dbh = $BDD->getConnection();
        $results = $dbh->prepare('SELECT * from users WHERE `mail` = ?');
        $results->execute(array($this->getMail()));
        $nbr = $results->rowCount();
        if ($nbr != 0) {
            $data = $results->fetch();

            $pass_verif = password_verify($this->getPassword(), $data["password"]);
            if (!$pass_verif) {
                echo "<p>Mot de passe erroné</p>";
            } else {
                $this->setAdmin($data["admin"]);
                $this->setFirstname($data["firstname"]);
                $this->setId($data["id"]);
                $this->setLastname($data["lastname"]);
                $this->setBirthdate($data["birthdate"]);
                $this->setPhonenumber($data["phonenumber"]);
                $_SESSION["user"] = $this;
                header("Location: index.php?page=account");
                exit();
            }

        } else {
            echo "<p>Adresse mail inconnue</p>";
        }
    }

    // Inscription de l'utilisateur avec un enregistrement dans la base de données après une vérification sur l'adresse mail
    public function register()
    {
        $BDD = new BDD();
        $dbh = $BDD->getConnection();

        $results = $dbh->prepare('SELECT `mail` from users WHERE `mail` = ?');
        $results->execute(array($_POST["mail"]));
        $nbr = $results->rowCount();
        if ($nbr == 0) {
            $this->setPassword($_POST["password"]);
            $hashed_pass = password_hash($this->getPassword(), PASSWORD_DEFAULT);
            $this->setMail($_POST["mail"]);
            $this->setFirstname($_POST["firstname"]);
            $this->setLastname($_POST["lastname"]);
            $stmt = $dbh->prepare("INSERT INTO users (`lastname`,`firstname`, `mail`, `password`) VALUES (:lname, :fname, :mail, :pass)");
            $stmt->bindParam(':lname', $this->lastname);
            $stmt->bindParam(':fname', $this->firstname);
            $stmt->bindParam(':mail', $this->mail);
            $stmt->bindParam(':pass', $hashed_pass);
            $stmt->execute();
            $results = $dbh->prepare('SELECT `id`, `admin` from users WHERE `mail` = ?');
            $results->execute(array($this->getMail()));
            $data = $results->fetch();
            $this->setId($data["id"]);
            $this->setAdmin($data["admin"]);
            $_SESSION["user"] = $this;
        } else {
            $_SESSION["used"] = "<p>Adresse mail déjà utilisée</p>";
        }
    }


    // Changement des informations d'utilisateur avec un nouveau mot de passe
    // Les modifications sont enregistrées en base de donnée
    public function changeWithPassword()
    {
        $hashed_pass = password_hash($_POST["new_password"], PASSWORD_DEFAULT);
        $BDD = new BDD();
        $dbh = $BDD->getConnection();
        $this->setFirstname($_POST["firstname"]);
        $this->setLastname($_POST["lastname"]);
        $this->setBirthdate($_POST["birthdate"]);
        $this->setPhoneNumber($_POST["number"]);
        $this->setPassword($_POST["new_password"]);
        $stmt = $dbh->prepare('UPDATE users SET `lastname` = :nlname, `firstname` = :nfname, `birthdate` = :bday, `phonenumber` = :phone, `password` = :npassword WHERE `id` = :id');
        $stmt->execute(array(':nlname' => $this->getLastname(), ':nfname' => $this->getFirstname(), ':bday' => $this->getBirthdate(), ':phone' => $this->getPhonenumber(), ':npassword' => $hashed_pass, ':id' => $this->getId()));
        $_SESSION["user"] = $this;
        header("Location: index.php?page=profile&change=valid");
        exit();
    }

    // Changement des informations d'utilisateur sans nouveau mot de passe
    // Les modifications sont enregistrées en base de donnée
    public function changeExceptPass()
    {
        $BDD = new BDD();
        $dbh = $BDD->getConnection();
        $this->setFirstname($_POST["firstname"]);
        $this->setLastname($_POST["lastname"]);
        $this->setBirthdate($_POST["birthdate"]);
        $this->setPhoneNumber($_POST["number"]);
        $stmt = $dbh->prepare('UPDATE users SET `lastname` = :nlname, `firstname` = :nfname, `birthdate` = :bday, `phonenumber` = :phone WHERE `id` = :id');
        $stmt->execute(array(':nlname' => $this->getLastname(), ':nfname' => $this->getFirstname(), ':bday' => $this->getBirthdate(), ':phone' => $this->getPhonenumber(), ':id' => $this->getId()));
        $_SESSION["user"] = $this;
        header("Location: index.php?page=profile&change=valid");
        exit();
    }

    // Change les droits d'un utilisateur avec enregistrement dans la BDD
    public function changeRights() {
        $this->setMail($_POST["mail"]);
        $BDD = new BDD();
        $dbh = $BDD->getConnection();
        $results = $dbh->prepare('SELECT * from users WHERE `mail` = ?');
        $results->execute(array($this->getMail()));
        $nbr = $results->rowCount();
        if ($nbr != 0) {
            $data = $results->fetch();
            $this->setAdmin($_POST["rights"]);
            $rights = $dbh->prepare('UPDATE `users` SET `admin` = :ad WHERE `mail` = :mail');
            $rights->execute(array(':ad' => $this->getAdmin(), ':mail' => $this->getMail()));
            echo "<p>Modifications enregistrées</p>";
        } else {
            echo "<p>Adresse mail inconnue</p>";
        }
    }
}
?>