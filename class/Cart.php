<?php

class Cart
{
    public $user_id;
    private $product_id;
    private $quantity;
    private $amount;

    // Constructeur de la classe Cart
    public function __construct($user_id)
    {
        $this->user_id = $user_id;
    }

    // Getter et setter 
    public function setProductId($product_id)
    {
        $this->product_id = $product_id;
    }

    public function getProductId()
    {
        return $this->product_id;
    }

    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;
    }

    public function getQuantity()
    {
        return $this->quantity;
    }

    public function setAmount($amount)
    {
        $this->amount = $amount;
    }

    public function getAmount()
    {
        return $this->amount;
    }
    // Fin des getter et setter


    // Création d'une nouvelle ligne dans la table Cart 
    public function newCart()
    {
        $product = $this->getProductId();
        $quant = $this->getQuantity();
        $this->cart_amount();
        $am = $this->getAmount();
        $BDD = new BDD();
        $dbh = $BDD->getConnection();
        $sth = $dbh->prepare('INSERT INTO `cart` (`user_id`, `product_id`,`quantity`,`amount`) VALUES (:user,:product, :quantity, :amount)');
        $sth->bindParam(':user', $this->user_id);
        $sth->bindParam(':product', $product);
        $sth->bindParam(':quantity', $quant);
        $sth->bindParam(':amount', $am);
        $sth->execute();
    }

    // Modification de la quantité et du montant d'une ligne dans la table Cart
    public function updateQuantity()
    {
        $this->cart_amount();
        $BDD = new BDD();
        $dbh = $BDD->getConnection();
        $sth = $dbh->prepare('UPDATE `cart` SET `quantity` = :quantity, `amount` = :amount WHERE `user_id` = :user AND `product_id` = :product');
        $sth->execute(array(':quantity' => $this->getQuantity(), ':amount' => $this->getAmount(), ':user' => $this->user_id, ':product' => $this->getProductId()));
    }

    // Ajout ou modification d'une ligne dans la table Cart
    public function addCart()
    {
        $BDD = new BDD();
        $dbh = $BDD->getConnection();
        $sth = $dbh->prepare('SELECT * from `cart` WHERE `user_id` = :user AND `product_id` = :product');
        $sth->execute(array(':user' => $this->user_id, ':product' => $this->getProductId()));
        $nbr = $sth->rowCount();
        if ($nbr == 0) {
            $this->newCart();
        } else {
            $this->updateQuantity();
        }
        $this->articlesOnCart();
    }

    // Vérirication du nombre d'articles dans la panier
    public function checkCart()
    {
        $BDD = new BDD();
        $dbh = $BDD->getConnection();
        $sth = $dbh->prepare('SELECT * FROM `cart` WHERE `user_id` = :user');
        $sth->execute(array(':user' => $this->user_id));
        $result = $sth->fetchAll();
        return $result;
    }

    // Création d'une variable dans la session avec le nombre d'articles dans le panier
    // Utilisé dans la page panier pour afficher la liste des articles ou un message en fonction du nombre d'articles dans le panier
    public function articlesOnCart()
    {
        $_SESSION["cart_number"] = 0;
        $articles = $this->checkCart();
        foreach ($articles as $article) {
            $_SESSION["cart_number"] += $article["quantity"];
        }
        return $_SESSION["cart_number"];
    }

    // Calcul du montant pour le produit grâce à son prix et à la quantité selectionnée
    public function cart_amount()
    {
        $BDD = new BDD();
        $dbh = $BDD->getConnection();
        $sth = $dbh->prepare('SELECT `price` from `product` WHERE `product_id` = :product_id');
        $sth->execute(array(':product_id' => $this->product_id));
        $result = $sth->fetch();
        $cart_amount = $result["price"] * ($this->getQuantity());
        $this->setAmount($cart_amount);
    }

    // Calcul du montant total du panier
    public function total_amount()
    {
        $BDD = new BDD();
        $dbh = $BDD->getConnection();
        $sth = $dbh->prepare('SELECT SUM(amount) as total FROM `cart` WHERE `user_id` = :user');
        $sth->execute(array(':user' => $this->user_id));
        $result = $sth->fetch();
        return $result["total"];
    }

    // Suppression d'une ligne dans le panier
    public function delete_cart()
    {
        $product = $this->getProductId();
        $BDD = new BDD();
        $dbh = $BDD->getConnection();
        $sth = $dbh->prepare('DELETE FROM `cart` WHERE `user_id` = :user AND `product_id` = :product');
        $sth->execute(array(':user' => $this->user_id, ':product' => $product));
    }
}
