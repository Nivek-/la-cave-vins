<?php 
// Redirection vers la page login si l'utilisateur n'est pas connecté
if(!isset($_SESSION["user"])) {
header("Location: index.php?page=login");
exit();
}
?>


<article class="container">
    <div class="account_content">
        <h1>Mon compte</h1>
        <div class="account_categories">
            <a href="index.php?page=profile" class="card_category">
                <i class="fas fa-address-card"></i>
                <h3>Profil</h3>
            </a>
            <a href="" class="card_category">
                <i class="fas fa-map-marker-alt"></i>
                <h3>Adresses</h3>
            </a>
            <a href="index.php?page=disconnect" class="card_category">
                <i class="fas fa-power-off"></i>
                <h3>Déconnexion</h3>
            </a>
        </div>
    </div>
</article>