<div class="top_footer">
    <div class="follow footer_categories">
        <div class="medias">
            <h3>Suivez-nous</h3>
            <div class='media_icons'>
                <a href=""><i class="fab fa-facebook-f"></i></a>
                <a href=""><i class="fab fa-twitter"></i></a>
                <a href=""><i class="fab fa-instagram"></i></a>
            </div>
        </div>
    </div>

    <div class="about footer_categories">
        <ul class="footer_links">
            <li>
                <h3>À propos</h3>
            </li>
            <li><a href="">Mentions légales</a></li>
            <li><a href="">Politique de confidentialité</a></li>
            <li><a href="">CGU</a></li>
            <li><a href="">CGV</a></li>
        </ul>
    </div>
    <div class="plus footer_categories">
        <ul class="footer_links">
            <li>
                <h3>Plus</h3>
            </li>
            <li><a href="">Plan du site</a></li>
            <li><a href="">FAQ</a></li>
        </ul>
    </div>
    <img src="assets/images/logo_footer.png" alt="logo de l'enseigne la cave à vins" class="logo" />
</div>
<div class="bottom_footer">
    <p>© 2020 Tous droits réservés La cave à vins</p>
</div>