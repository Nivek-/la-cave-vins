<?php

$admin = new Users();
$admin = $_SESSION["user"];
if (!isset($_SESSION["user"]) || (isset($_SESSION["user"]) && $admin->getAdmin() != 1)) {
    header("Location: index.php?page=home");
    exit();
}

?>


<article class="container">
    <div class="change_products">

        <!-- Formulaire d'ajout d'un nouveau produit -->

        <div class="add_remove">
            <h2>Ajouter un produit</h2>

            <form method="POST" action="index.php?page=change_products">
                <div class="form_product">
                    <label for="name">Nom du produit</label>
                    <input type="text" name="name" id="name" />
                </div>
                <div class="form_product">
                    <label for="category">Catégorie</label>
                    <select name="category" id="category">
                        <option value="" selected>Sélectionnez une option</option>

                        <?php
// Création des options du sélecteur avec la liste des catégories
$BDD = new BDD();
$dbh = $BDD->getConnection();
$sth = $dbh->prepare("SELECT `name` from `category` WHERE `name` != 'Tous nos produits'");
$selection = $sth->execute(array());
foreach ($sth as $row) {
    $cat = new Category($row["name"]);
    echo "<option value='" . $cat->name . "'>$cat->name</option>";
}
?>
                    </select>
                </div>
                <div class="form_product">
                    <label for="origin">Origine</label>
                    <select name="origin" id="origin">
                        <option value="" selected>Sélectionnez une option</option>
                        <?php
// Création des options du sélecteur avec la liste des origines
$stmt = $dbh->prepare("SELECT `name` from `origin` ");
$results = $stmt->execute(array());
foreach ($stmt as $row) {
    $or = new Origin($row["name"]);
    echo "<option value='" . $or->name . "'>$or->name</option>";
}
?>
                    </select>
                </div>
                <div class="form_product">
                    <label for="year">Millésime</label>
                    <input type="number" name="year" id="year" min="1700" max="9999"value=""/>
                </div>
                <div class="form_product">
                    <label for="prix">Prix</label>
                    <input type="number" name="price" id="price" min="0" value=""/>
                </div>
                <div class="form_product">
                    <label for="color">Robe</label>
                    <select name="color" id="color">
                        <option value="" selected>Sélectionnez une option pour un vin</option>
                        <option value="Vins rouges">Rouge</option>
                        <option value="Vins blancs">Blanc</option>
                        <option value="Vins rosés">Rosé</option>
                    </select>
                </div>
                <div class="form_product">
                    <label for="describe">Description du produit</label>
                    <textarea name="describe" id="describe" value=""></textarea>
                </div>
                <div class="form_product">
                    <label for="stock">Stock</label>
                    <input type="number" name="stock" id="stock" min="0"/>
                </div>
                <div class="form_product">
                    <label for="image">Nom et extension de l'image (format de 400 x 533 optimisé pour le site)</label>
                    <input type="text" name="image" id="image" />
                </div>
                <input type="submit" value="Ajouter un produit" name="submit_btn" class="valid_btn">
            </form>
            <?php

if (isset($_POST["submit_btn"]) && $_POST["submit_btn"] == "Ajouter un produit") {
    // Ajout d'un nouveau produit dans la BDD après véification des informations saisies
    $product = new Product();
    $product->setCategory($_POST["category"]);
    $product->setName($_POST["name"]);
    $product->setYear($_POST["year"]);
    $product->setPrice($_POST["price"]);
    $product->setDescribe($_POST["describe"]);
    $product->setImage($_POST["image"]);
    $product->setStock($_POST["stock"]);
    $i = 0;
    // Augmente la valeur de i pour chaque champs vide ou null
    foreach ($_POST as $key => $value) {
        if ($value == "" || $value == null) {
            $i++;
        }
    }

    // if ($_POST["stock"] == "" || $_POST["color"] == "" || $_POST["origin"] == "") {
    //     $i++;
    // }

    // Vérification des champs vides grâce à i
    if ($i > 0) {
        if ($_POST["color"] != "" || $_POST["origin"] != "") {
            echo "<p>Il faut remplir tous les champs sauf la robe et l'origine s'il s'agit d'un champagne</p>";
        } else if ($_POST["color"] == "" && $_POST["origin"] == "") {
            if ($product->getCategory() != "Champagne") {
                echo "<p>Sélectionnez une robe et une origine pour le vin</p>";
            } else {
                $product->setColor($_POST["color"]);
                $product->setOrigin($_POST["origin"]);
                $product->save();
                echo "<p>Produit ajouté !</p>";
            }
        } else {
            echo "<p>Sélectionnez une robe et une origine pour le vin</p>";
        }
    } else {
        if ($product->getCategory() == "Champagne") {
            echo "<p>Ne pas sélectionner de robe et d'origine pour un champagne</p>";
        } else {
            $product->setColor($_POST["color"]);
            $product->setorigin($_POST["origin"]);
            $product->save();
            echo "<p>Produit ajouté !</p>";
        }
    }

}
?>
        </div>
        <div class="add_remove">

        <!-- Formulaire d'ajout d'un nouveau produit -->

            <h2>Supprimer un produit</h2>
            <form method="POST" action="index.php?page=change_products">
                <div class='form_product'>
                <label for="remove_name">Nom du produit</label>
                <input type="text" name="remove_name" id="remove_name" />
                </div>
                <input type="submit" value="Supprimer un produit" name="submit_btn" class="valid_btn">
            </form>
            <?php
// Suppression d'un produit de la BDD après vérification de l'existance du produit
if (isset($_POST["submit_btn"]) && $_POST["submit_btn"] == "Supprimer un produit") {
    $select = $dbh->prepare("SELECT `name` from `product` WHERE `name` = ?");
    $select->execute(array($_POST["remove_name"]));
    $nbr = $select->rowCount();
    if ($nbr == 0) {
        echo "<p>Produit inconnu. Veuillez entrer le nom complet indiqué sur sa fiche produit</p>";
    } else {
        $remove = $dbh->prepare("DELETE from `product` WHERE `name` = ?");
        $remove->execute(array($_POST["remove_name"]));
        echo "<p>Produit supprimé</p>";
    }

}
?>
        </div>
    </div>
</article>