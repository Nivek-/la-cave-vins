
<article class="container">
    <div class="cart_page">
    <h2>Mon panier</h2>
    <div class="articles">

    <?php
    // Affichage des articles du panier avec la quantité si l'utilisateur est connecté
    // S'il n'est pas connecté ou s'il n'y a pas de produit dans son panier un message apparaît
if (isset($_SESSION["user"])) {
    $cart = new Cart($_SESSION["user"]->getId());
    $cart_articles = $cart->articlesOnCart();
    $total_amount = $cart->total_amount();
    // Vérification du nombre d'articles dans le panier 
    if ($cart_articles == 0) {
        echo "<p>Ajoutez des articles à votre panier pour le visualiser</p>";
    } else {
        echo "<table><thead><tr>";
        echo "<td class='border_none'></td>";
        echo "<td>Nom</td>";
        echo "<td>Quantité</td>";
        echo "<td>Prix</td>";
        echo "<td>Action</td>";
        echo "</tr></thead>";
        echo "<tbody>";
        
        $articles = $cart->checkCart();
        // Affichage de chaque article du panier avec une photo, le nom, la quantité, le prix et un bouton pour le retirer
        foreach($articles as $article) {
            $product = new Cart($article["user_id"]);
            $product->setProductId($article["product_id"]);
            $product->setQuantity($article["quantity"]);
            $product->cart_amount();


            $showed_product = new Product();
            $showed_product->showArticle($product->getProductId());
            echo "<tr>";
            echo "<td><img src='assets/images/" . $showed_product->getImage() . "' alt='Bouteille de " . $showed_product->getName() . " " . $showed_product->getYear() . "' class='wine_bottle' /></td>";
            echo "<td>" . $showed_product->getName() . "</td>";
            echo "<td>";
            echo "<form method='POST' action='index.php?page=cart'>";
            echo "<input class='quantity' type='number' name='quantity' value='".$product->getQuantity()."' min='1' max='".$showed_product->getStock()."' />";
            echo "<input type='hidden' value='".$product->getProductId()."'>";
            echo "</form>";
            echo "</td>";
            echo "<td><span class='amount'>" . $product->getAmount(). "</span> € TTC</td>";
            echo "<td><i class='fas fa-trash delete'></i></td>";
            echo "</tr>";
        }

        echo "<tr>";
        echo "<td></td>";
        echo "<td></td>";
        echo "<td>Montant total</td>";
        // Affichage du montant total du panier
        echo "<td><span id='total_amount'>".$total_amount."</span> € TTC</td>";
        echo "<td></td>";
        echo "</tr>";
        echo "</tbody>";
        echo "</table>";
    }
} else {
    echo "<p>Il faut être connecté pour voir son panier</p>";
}
?>

    </div>
    </div>
</article>



