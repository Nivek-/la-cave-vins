<?php
// Chargement de toutes les classes
require_once "class/Bdd.php";
require_once "class/Product.php";
require_once "class/Origin.php";
require_once "class/Category.php";
require_once "class/Color.php";
require_once "class/Users.php";
require_once "class/Cart.php";
require_once __DIR__.'/vendor/autoload.php';

// Lancement de la sessions
session_start();

// Création d'un menu avec les catérogies, les origines et les robes importées depuis la BDD
function createNav()
{
    echo "<ul>";
    $BDD = new BDD();
    $dbh = $BDD->getConnection();
    $sth = $dbh->query('SELECT  `name` from `category`');
    foreach ($sth as $row) {
        $cat = new Category($row["name"]);
        $cat_name = $cat->name;
        $cat_url = str_replace(" ", "%20", $cat_name);
        echo "<li>";
        echo "<h2>";
        echo "<a href='index.php?page=products&category=" . $cat_url . "'>" . $cat_name . "</a>";
        echo "</h2>";

        if ($cat_name != "Tous nos produits") {
            $results = $dbh->prepare("SELECT `origin`.`name`, `origin`.`category_name` from `origin` WHERE `category_name` = ?");
            $selection = $results->execute(array($cat_name));
            $nb = $results->rowCount();
            if ($nb != 0) {
                echo "<ul class='menu_subcategories'>";
                foreach ($results as $origin_row) {
                    $or = new Origin($origin_row["name"]);
                    $or_name = $or->name;
                    $or_url = str_replace(" ", "%20", $or_name);
                    echo "<li>";
                    echo "<h3>";
                    echo "<a href='index.php?page=products&origin=" . $or_url . "'>" . $or_name . "</a>";
                    echo "</h3>";
                    echo "</li>";

                }
                echo "</ul>";

            }
        } else if ($cat_name == "Tous nos produits") {
            $color = ["Vins rouges", "Vins blancs", "Vins rosés"];
            echo "<ul class='menu_subcategories'>";
            for ($i = 0; $i < count($color); $i++) {
                $col_url = str_replace(" ", "%20", $color[$i]);
                echo "<li>";
                echo "<h3>";
                echo "<a href='index.php?page=products&color=" . $col_url . "'>" . $color[$i] . "</a>";
                echo "</h3>";
                echo "</li>";
            }
            echo "</ul>";
        }
        echo "</li>";

    }
    // Présence d'un lien vers la page admin si l'utilisateur a les droits
    if(isset($_SESSION["user"])) {
        $admin = new Users();
        $admin = $_SESSION["user"];
        if(isset($_SESSION["user"]) && $admin->getAdmin() == 1) {
            echo "<li class='admin'>";
            echo "<h2>";
            echo "<a href='index.php?page=admin'>Admin</a>";
            echo "</h2>";
            echo "</li>";
        }
    }
    echo "</ul>";
}

// Affichage de tous les produits dans la page products.php quelle que soit leur catégorie, leur origine et leur couleur
function showAllProducts()
{
    $BDD = new BDD();
    $dbh = $BDD->getConnection();
    $sth = $dbh->query('SELECT  `product_id`, `name`,`year`,`price`,`describe`, `color`,`origin`,`image`, `stock` from `product`');
    while ($data = $sth->fetch(PDO::FETCH_OBJ)) {
        echo "<div class='products_bloc' id='".$data->product_id."'>";
        echo "<a href='index.php?page=detail&product=" . $data->product_id . "' class='product_card'>";
        echo "<img src='assets/images/" . $data->image . "' alt='Bouteille de " . $data->name . " " . $data->year . "' class='wine_bottle' />";
        echo "<h4>" . $data->name . " " . $data->year . "</h4>";
        echo "<p>" . $data->price . " € TTC</p>";
        echo "</a>";
        echo "<div class='cart_status'>";
        if ($data->stock > 0) {
            echo "<div class='stock'>";
            echo "<i class='fas fa-check-circle'></i>";
            echo "<p>En stock (<span class='stock_value'>".$data->stock."</span>)</p>";
            echo "</div>";
            echo "<a class='addcart_button add-cart'>ajouter au panier</a>";

        } else {
            echo "<div class='stock'>";
            echo "<i class='fas fa-times-circle'></i>";
            echo "<p>Rupture de stock</p>";
            echo "</div>";
            echo "<a href='' class='addcart_button unclick'>ajouter au panier</a>";
        }
        echo "</div>";
        echo "</div>";
    }
}
