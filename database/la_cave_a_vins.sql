-- MySQL dump 10.13  Distrib 5.7.29, for Linux (x86_64)
--
-- Host: localhost    Database: la_cave_a_vins
-- ------------------------------------------------------
-- Server version	5.7.29-0ubuntu0.18.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Current Database: `la_cave_a_vins`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `la_cave_a_vins` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `la_cave_a_vins`;

--
-- Table structure for table `cart`
--

DROP TABLE IF EXISTS `cart`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cart` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `amount` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cart`
--

LOCK TABLES `cart` WRITE;
/*!40000 ALTER TABLE `cart` DISABLE KEYS */;
INSERT INTO `cart` VALUES (8,3,5,2,0),(9,3,1,1,0),(10,3,2,6,0),(11,1,1,1,80),(12,1,2,2,210),(13,1,7,1,80),(14,1,8,2,700);
/*!40000 ALTER TABLE `cart` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `category`
--

DROP TABLE IF EXISTS `category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `category` (
  `category_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `category`
--

LOCK TABLES `category` WRITE;
/*!40000 ALTER TABLE `category` DISABLE KEYS */;
INSERT INTO `category` VALUES (1,'Vins de France'),(2,'Vins du monde'),(3,'Champagne'),(4,'Tous nos produits');
/*!40000 ALTER TABLE `category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `category_origin`
--

DROP TABLE IF EXISTS `category_origin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `category_origin` (
  `product_id` int(11) unsigned NOT NULL,
  `origin_id` int(11) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `category_origin`
--

LOCK TABLES `category_origin` WRITE;
/*!40000 ALTER TABLE `category_origin` DISABLE KEYS */;
/*!40000 ALTER TABLE `category_origin` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `origin`
--

DROP TABLE IF EXISTS `origin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `origin` (
  `origin_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `category_name` varchar(100) NOT NULL,
  PRIMARY KEY (`origin_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `origin`
--

LOCK TABLES `origin` WRITE;
/*!40000 ALTER TABLE `origin` DISABLE KEYS */;
INSERT INTO `origin` VALUES (1,'Bordeaux','Vins de France'),(2,'Bourgogne','Vins de France'),(3,'Jura','Vins de France'),(4,'Languedoc-Roussillon','Vins de France'),(5,'RhÃ´ne','Vins de France'),(6,'Allemagne','Vins du monde'),(7,'Argentine','Vins du monde'),(8,'Italie','Vins du monde');
/*!40000 ALTER TABLE `origin` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product`
--

DROP TABLE IF EXISTS `product`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product` (
  `product_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `category` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `year` int(4) NOT NULL,
  `price` int(11) NOT NULL,
  `describe` text NOT NULL,
  `color` varchar(255) DEFAULT NULL,
  `origin` varchar(255) DEFAULT NULL,
  `stock` int(11) unsigned NOT NULL,
  `image` varchar(255) NOT NULL,
  PRIMARY KEY (`product_id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product`
--

LOCK TABLES `product` WRITE;
/*!40000 ALTER TABLE `product` DISABLE KEYS */;
INSERT INTO `product` VALUES (1,'Vins de France','ChÃ¢teau Brane-Cantenac Margaux',1989,80,'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam aliquam a lorem vel rutrum. Morbi rhoncus tellus libero, a porta erat porta ultrices. Praesent mattis faucibus leo vitae tristique. Nullam mattis nunc ac velit mattis, id elementum arcu ullamcorper. Curabitur lacinia at est nec malesuada.','Vins rouges','Bordeaux',5,'brane_cantenac_margaux.png'),(2,'Vins de France','ChÃ¢teau Duhard-Milon Pauillac',2002,105,'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam aliquam a lorem vel rutrum. Morbi rhoncus tellus libero, a porta erat porta ultrices. Praesent mattis faucibus leo vitae tristique. Nullam mattis nunc ac velit mattis, id elementum arcu ullamcorper. Curabitur lacinia at est nec malesuada.','Vins rouges','Bordeaux',10,'duhart_milon_pauillac.png'),(3,'Vins de France','ChÃ¢teau Labegorce Margaux',2002,37,'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam aliquam a lorem vel rutrum. Morbi rhoncus tellus libero, a porta erat porta ultrices. Praesent mattis faucibus leo vitae tristique. Nullam mattis nunc ac velit mattis, id elementum arcu ullamcorper. Curabitur lacinia at est nec malesuada.','Vins rouges','Bordeaux',50,'labegorce_margaux.png'),(5,'Vins de France','ChÃ¢teau PrieurÃ©-Lichine Margaux',1995,50,'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam aliquam a lorem vel rutrum. Morbi rhoncus tellus libero, a porta erat porta ultrices. Praesent mattis faucibus leo vitae tristique. Nullam mattis nunc ac velit mattis, id elementum arcu ullamcorper. Curabitur lacinia at est nec malesuada.','Vins rouges','Bordeaux',15,'prieure_lichine_margaux.png'),(7,'Vins de France','ChÃ¢teau Rauzan-Gassies Margaux',2000,80,'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam aliquam a lorem vel rutrum. Morbi rhoncus tellus libero, a porta erat porta ultrices. Praesent mattis faucibus leo vitae tristique. Nullam mattis nunc ac velit mattis, id elementum arcu ullamcorper. Curabitur lacinia at est nec malesuada.','Vins rouges','Bordeaux',40,'rauzan_gassies_margaux.png'),(8,'Vins de France','D\'Yquem Y Lur-Saluces',1988,350,'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam aliquam a lorem vel rutrum. Morbi rhoncus tellus libero, a porta erat porta ultrices. Praesent mattis faucibus leo vitae tristique. Nullam mattis nunc ac velit mattis, id elementum arcu ullamcorper. Curabitur lacinia at est nec malesuada.','Vins blancs','Bordeaux',2,'y_lur_saluces.png'),(9,'Vins de France','Domaine Chavy-Chouet Meursault',2015,69,'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam aliquam a lorem vel rutrum. Morbi rhoncus tellus libero, a porta erat porta ultrices. Praesent mattis faucibus leo vitae tristique. Nullam mattis nunc ac velit mattis, id elementum arcu ullamcorper. Curabitur lacinia at est nec malesuada.','Vins blancs','Bourgogne',70,'chavy_chouet_meursault.png'),(11,'Vins de France','Morey-Saint-Denis Dufouleur',1985,30,'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam aliquam a lorem vel rutrum. Morbi rhoncus tellus libero, a porta erat porta ultrices. Praesent mattis faucibus leo vitae tristique. Nullam mattis nunc ac velit mattis, id elementum arcu ullamcorper. Curabitur lacinia at est nec malesuada.','Vins rouges','Bourgogne',30,'morey_saint_denis_dufouleur.png'),(12,'Vins de France','RomanÃ©e-Saint-Vivant Marey-Monge',1973,1549,'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam aliquam a lorem vel rutrum. Morbi rhoncus tellus libero, a porta erat porta ultrices. Praesent mattis faucibus leo vitae tristique. Nullam mattis nunc ac velit mattis, id elementum arcu ullamcorper. Curabitur lacinia at est nec malesuada.','Vins rouges','Bourgogne',2,'romanee_saint_vivant_marey_monge.png'),(13,'Champagne','Bollinger La grande annÃ©e',2007,100,'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam aliquam a lorem vel rutrum. Morbi rhoncus tellus libero, a porta erat porta ultrices. Praesent mattis faucibus leo vitae tristique. Nullam mattis nunc ac velit mattis, id elementum arcu ullamcorper. Curabitur lacinia at est nec malesuada.','','',0,'bollinger_la_grande_annee.png'),(14,'Champagne','Dom PÃ©rignon MoÃ«t & Chandon',2000,650,'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam aliquam a lorem vel rutrum. Morbi rhoncus tellus libero, a porta erat porta ultrices. Praesent mattis faucibus leo vitae tristique. Nullam mattis nunc ac velit mattis, id elementum arcu ullamcorper. Curabitur lacinia at est nec malesuada.','','',3,'dom_perignon_moet_chandon.png'),(16,'Vins de France','ChÃ¢teau Pontet-Canet Pauillac',2001,100,'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam aliquam a lorem vel rutrum. Morbi r','Bordeaux',NULL,50,'pontet_canet_pauillac.png');
/*!40000 ALTER TABLE `product` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `lastname` varchar(255) NOT NULL,
  `firstname` varchar(255) NOT NULL,
  `mail` varchar(255) DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `birthdate` varchar(255) DEFAULT NULL,
  `admin` int(1) NOT NULL DEFAULT '0',
  `phonenumber` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=74 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'admin','admin','admin@admin.com','$2y$10$tJAUlM13/cm4Qw9i2bmBtOp7bmqVUCPx.M/liX3qq1QaZH6F3ttne',NULL,1,NULL),(2,'user','user','user@user.com','$2y$10$j.VTHzN2eMqZaOjZVkKWtuWbeYq./YPAduqT4pqN1n7j2uIev9WyS',NULL,0,NULL),(3,'test','test','test@test.com','$2y$10$kZOMlO2uE2bMyY1DwpryfeZR3p2Qrbb0rYR9iRteHseaWx4Yzt3SC','1991-08-09',1,'0606090908'),(4,'new user','new user','new_user@new.com','$2y$10$YMRdNvXoZn8dRVIJZqU4Pu25FnHA0IqyP03/IwYlx1KFoqzKHFZ6a',NULL,0,NULL),(5,'bonjour','quoi','quoi@quoi.com','$2y$10$NY.gvky89iyuNejYf4ygSOzFCg6uqxvQoKUC3lLSR6D7TMXy/wn0.','',0,''),(6,'hello','Michel','hello@hello.com','$2y$10$baB7KV/UIqVjwv/.a/mjPePhFVuhA3uf9ZxEIPbdL.FgbWSlrh5bq',NULL,0,NULL),(7,'aaa','aaa','a@a.com','$2y$10$dcs/Xl5YchAzEYkFA/8V.e7YaJD96BA9R/DmWnQRAXsAqju5sx.6G',NULL,0,NULL),(8,'bbb','b','b@b.com','$2y$10$e3jlFigzNWAhT/0..B4IoOGnstl2qRY1T5AwPYhK/Lwy5Yo4Swqg6',NULL,0,NULL),(9,'a','a','test@test.com','$2y$10$ybstQJAcf3YrfCkiXeMqiOVNcFIzbRTijsDi5i.ZkprukECMZz59a',NULL,1,NULL),(10,'aaa','aaaa','test@test.com','$2y$10$faaPJGbv6czMFL3r9BL3Ju.dVqBxgoNcvNQspEIgM5Pz5mWGoWsgu',NULL,1,NULL),(11,'aaa','aaaa','test@test.com','$2y$10$gQaPlBTs2bRgpMtDmZZhHuhycFsIhwa4XLyQ.hsjmVbD8zw3m2xFa',NULL,1,NULL),(62,'c','c','c@c.com','$2y$10$RxlhOJoDYX5KucBZmwQUGedbxqBVmaw76.hSzNRiYJY17b5zG8CtC',NULL,0,NULL),(63,'d','d','d@d.com','$2y$10$oNbLvnbqGvNl335/ZcOQEOCEC/gdYlZmreA34IMB2QCl8AZwJ0tDi',NULL,0,NULL),(64,'f','f','f@f.com','$2y$10$RyNc8ahQyYIxX9Kgel14De8V/OaB.343fRvT5tTv5uPtSHSoVjbsq',NULL,0,NULL),(65,'g','g','g@g.com','$2y$10$FYgpUX2EoklW4wQAvssw2ubDnCF0gtB.PjORMm19Uv2EHYtYg6Syq',NULL,0,NULL),(66,'h','h','h@h.com','$2y$10$7zkVVHFxvwzAKUzyP2hf9.vcg7yOC1x6KYfMbVvt9LU09pWnM0fW.',NULL,0,NULL),(67,'v','v','v@v.com','$2y$10$5CZuMVw/Mcpl.n6VJGXX6.2C8ZvSB7xCeccNEglaKY76pne.YTi1.',NULL,0,NULL),(72,'e','e','e@e.com','$2y$10$tIfbMbMpOYFJhE2c7snT9eOarbHnfqAJffghZWPF/yFbodVz6ZyXW','',0,''),(73,'i','i','i@i.com','$2y$10$J/tJZR1cCxN2jbFtLXWQmu1Bhh4cNlFw/0YXnBmUC6JON6jj2f69C',NULL,0,NULL);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-03-23  8:56:58
