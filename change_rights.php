<?php

$admin = new Users();
$admin = $_SESSION["user"];
if (!isset($_SESSION["user"]) || (isset($_SESSION["user"]) && $admin->getAdmin() != 1)) {
    header("Location: index.php?page=home");
    exit();
}

?>

<article class="container">
    <div class="change_rights">
        <h2>Modifier les droits d'un utilisateur</h2>

        <!-- Formulaire de changement de droits pour un utilisateur -->

        <form method="POST" action="index.php?page=change_rights">
            <div class="form_rights">
                <label for="mail">Adresse mail de l'utilisateur</label>
                <input type="mail" name="mail" id="mail" />
            </div>
            <div class="form_rights">
                <label for="rights">Statut à donner</label>
                <select name="rights" id="category">
                    <option value="1">Administrateur</option>
                    <option value="0">Utilisateur</option>
                </select>
            </div>
            <input type="submit" value="Modifier les droits" name="submit_btn" class="valid_btn">
        </form>

        <?php

        // Changement des droits de l'utilisateur
        if(isset($_POST["submit_btn"]) && $_POST["submit_btn"] == "Modifier les droits")
        {
            $admin = new Users();
            $admin->changeRights();
        }
        ?>

    </div>
</article>