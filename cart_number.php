<?php
require_once "autoload.php";
//Calcul dynamique du nombre de produits dans le panier via une requête AJAX
if (isset($_SESSION["user"])) {
    $cart = new Cart($_SESSION["user"]->getId());
    $result = $cart->articlesOnCart();
    echo json_encode(array('data' => $result));
}
