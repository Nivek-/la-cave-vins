 <div class="top_header">
     <div class="first_bloc">
         <a href="index.php?page=home"><img src="assets/images/logo_header.png" alt="logo de l'enseigne La cave à vins"
                 class="logo" /></a>
         <div class="searchbar">
             <input type="text" name="search" id="search" placeholder="Rechercher">
             <i class="fas fa-search"></i>
         </div>
     </div>
     <div class="header_buttons">
         <div class="icon_buttons">
             <a href=""><i class="far fa-comment-dots"></i></a>
             <?php

    // Redirection de l'utilisateur vers son compte s'il est connecté sinon vers la page login
    if(isset($_SESSION["user"])) {
        $welcome = $_SESSION["user"];
        echo "<a href='index.php?page=account'><i class='fas fa-user-circle'></i></a>";
    } else {
    echo "<a href='index.php?page=login'><i class='fas fa-user-circle'></i></a>";
}
?>
             <div class="cart">
                 <a href="index.php?page=cart">
                     <i class="fas fa-shopping-cart"></i>
                     <?php
// Compteur du panier mis à jour dynamiquement
if (isset($_SESSION["user"])) {
    echo "<p id='cart_count'></p>";
} else {
    echo "<p id='cart_count'>0</p>";
}
?>
                 </a>
             </div>
         </div>
         <?php
         // Ajout d'un message personnalisé avec le prénom lorsque l'utilisateur est connecté
    if (isset($_SESSION["user"])) {
        echo "<p>Bonjour " . $welcome->getFirstname() . "</p>";
    }
?>
     </div>
 </div>
 <nav>
     <?php

// Appel de la fonction depuis autoload.php pour créer la navbar
createNav();
?>

 </nav>