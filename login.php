<?php
if (isset($_SESSION["user"])) {
    // Redirection vers la page compte si l'utilisateur est déjà connecté
    header("Location: index.php?page=account");
    exit();
}
?>


<article class="container">
    <div class="user_log">
        <div class="log_bloc">
            <h2>Déjà inscrit?</h2>

            <!-- Formulaire de connexion -->

            <form method="POST" action="index.php?page=login">
                <div class="form_cat">
                    <label for="mail_log">Adresse mail</label>
                    <input type="email" name="mail_log" id="mail_log" />
                </div>
                <div class="form_cat">
                    <label for="password_log">Mot de passe</label>
                    <input type="password" name="password_log" id="password_log" />
                </div>
                <input type="submit" value="Connexion" name="submit_btn" class="valid_btn">
            </form>
            <?php

// Connexion de l'utilisateur après vérification des champs saisis
if (isset($_POST["submit_btn"])) {
    if ($_POST["submit_btn"] == "Connexion") {
        if ($_POST["mail_log"] == null || $_POST["password_log"] == null) {
            echo "<p>Veuillez remplir les deux champs pour vous connecter</p>";
        } else {
            $user = new Users();
            $user->setMail($_POST["mail_log"]);
            $user->setPassword($_POST["password_log"]);
            $user->login();
        }
    }
}
?>
        </div>
        <div class="log_bloc">
            <h2>Créer votre compte</h2>
            <?php

// Inscription de l'utilisateur avec ajout d'une nouvelle ligne dans la BDD après vérification des champs saisis
if (isset($_POST["submit_btn"])) {
    if ($_POST["submit_btn"] == "Inscription") {
        if ($_POST["firstname"] == null || $_POST["lastname"] == null || $_POST["mail"] == null || $_POST["password"] == null) {
            $champs = "<p>Veuillez saisir tous les champs</p>";
        } else {
            $user = new Users();
            $user->register();
            if (isset($_SESSION["user"])) {

                // Envoi de mail


                // $transport = (new Swift_SmtpTransport('	smtp.mailtrap.io', 25))
                //     ->setUsername('5f134e188f48ec')
                //     ->setPassword('9f2293fc8b5362')
                // ;
                // $mailer = new Swift_Mailer($transport);

                // $message = (new Swift_Message('Inscription au site la cave à vins'))
                //     ->setFrom(['testswiftmailerpop@gmail.com' => 'Kévin Marquegnies'])
                //     ->setTo([$_POST["mail"] => $_POST["lastname"].' '.$_POST["firstname"]])
                //     ->setBody('Bienvenue sur le site La cave à vins')
                // ;

                // $result = $mailer->send($message);


                header("Location: index.php?page=account");
                exit();
            }
        }
    }
}
?>
            <!-- Formulaire d'inscription -->

            <form method="POST" action="index.php?page=login">
                <div class="form_cat">
                    <label for="lastname">Nom</label>
                    <input type="text" name="lastname" id="last_name"/>
                </div>
                <div class="form_cat">
                    <label for="firstname">Prénom</label>
                    <input type="text" name="firstname" id="first_name"/>
                </div>
                <div class="form_cat">
                    <label for="mail">Adresse mail</label>
                    <input type="email" name="mail" id="mail"/>
                </div>
                <div class="form_cat">
                    <label for="password">Mot de passe</label>
                    <input type="password" name="password" id="password"/>
                </div>
                <input type="submit" value="Inscription" name="submit_btn" class="valid_btn">
            </form>
<?php
if (isset($champs)) {
    echo $champs;
}
if (isset($_SESSION["used"])) {
    echo $_SESSION["used"];
    unset($_SESSION["used"]);
}
?>


        </div>
    </div>
</article>