var menu = document.getElementsByTagName("nav")[0];

var fixe = menu.offsetTop;

var topScroll = document.getElementsByClassName("fa-chevron-circle-up")[0];

// Le menu suit la page au scroll
window.addEventListener("scroll", function () {
  if (window.pageYOffset >= fixe) {
    menu.classList.add("fixed");
    topScroll.style.visibility = "visible";
  } else {
    menu.classList.remove("fixed");
    topScroll.style.visibility = "hidden";
  }
});

$('document').ready(function () {
  updateCartNumber();

  // Permet l'ouverture du popin avec les informations du produit qui souhaite être ajouté au panier
  $('.add-cart').on('click', function () {
    var titleProduct = $(this).parent().parent().find('a').find('h4').html();
    var imgProduct = $(this).parent().parent().find('a.product_card img').attr("src");
    var stockProduct = $(this).parent().find('.stock p .stock_value').html();
    var idProduct = $(this).parent().parent().attr("id")
    $('#modalProduct .modal-header h5').html(titleProduct);
    $('#modalProduct .modal-body .col-4').html('<img src="' + imgProduct + '">');
    $('#quantity').attr('max', stockProduct)
    $('#quantity').val(1)
    $('#product_id').val(idProduct)
    $('#product_added').css("display", "none");
    $('#modalProduct').modal('show');
  });

  // Désactive l'écouteur sur le bouton
  $('.btn-add-to-card').off('click')

  // Ajout de l'élément au panier avec la quantité sélectionnée
  $('.btn-add-to-card').on('click', function () {
    var ProductId = $('#product_id').val()
    var Quantity = $('#quantity').val()
    $.ajax({
      url: 'addcart.php',
      type: 'POST',
      data: {
        // Envoie en POST les l'id et la quantité du produit
        ProductId: ProductId,
        Quantity: Quantity
      },
      dataType: 'JSON',
      success: function (callback, statut) {
        if (callback.success) {
          // Apparition d'une phrase indiquant l'ajout au panier pour l'utilisateur et mise à jour du nombre de produits dans le panier
          $('#product_added').css("display", "block")
          updateCartNumber();
        }
      }
    });
  })

  // Mise à jour dynamique du nombre d'articles dans le panier (affiché dans le header)
  function updateCartNumber() {
    $.ajax({
      url: 'cart_number.php',
      type: 'POST',
      data: {},
      dataType: 'JSON',
      success: function (callback, statut) {
        var currentQtt = $('#cart_count').html();
        currentQtt = callback.data;
        $('#cart_count').html(currentQtt);
      }
    });
  }

  // Au changement de quantité d'un produit dans le panier
  // Le montant pour le produit, le montant total et le nombre d'articles dans le panier est mis à jour
  $(".quantity").on("change", function () {
    var quantity = $(this).val()
    var productId = $(this).parent().children().last().val()
    var productAmount = $(this).parent().parent().parent().find(".amount")
    $.ajax({
      url: 'change_quantity.php',
      type: 'POST',
      data: {
        // Envoie en POST les l'id et la quantité du produit
        quantity: quantity,
        productId: productId
      },
      dataType: 'JSON',
      success: function (callback, statut) {
        $("#total_amount").html(callback.data)
        productAmount.html(callback.amount)
        updateCartNumber()
      }
    });
  })

  // Au clic sur le bouton suppression
  // L'article est retiré du panier dans la base de donnée et le montant total est recalculé
  // L'article est supprimé dynamiquement de la page panier
  $(".delete").on("click", function () {
    var productId = $(this).parent().parent().find("form input[type=hidden]").val()
    var productBlock = $(this).parent().parent()
    $.ajax({
      url: 'removecart.php',
      type: 'POST',
      data: {
        // Envoie en POST les l'id du produit
        productId: productId
      },
      dataType: 'JSON',
      success: function (callback, statut) {
        $("#total_amount").html(callback.data)
        updateCartNumber()
        productBlock.remove()
      }
    });
  })
})