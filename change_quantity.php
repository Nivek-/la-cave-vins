<?php
require_once("autoload.php");

// Changement dynamique de la quantité d'un produit du panier et calcul du nouveau montant total via une requête AJAX et
if(isset($_POST["productId"])) {
    $user = new Users();
    $user = $_SESSION["user"];
    $cart = new Cart($user->getId());
    $cart->setProductId($_POST["productId"]);
    $cart->setQuantity($_POST["quantity"]);
    $cart->updateQuantity();
    $result = $cart->total_amount();
    $cart_amount = $cart->getAmount();
    echo json_encode(array('data'=>$result, 'amount'=> $cart_amount));
}
?>