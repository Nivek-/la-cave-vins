<?php
require_once "autoload.php";
?>
<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>La cave à vins</title>
    <link
        href="https://fonts.googleapis.com/css?family=Raleway:100,200,300,400,400i,500,500i,600,700,800,900|Roboto:100,300,400,400i,500,500i,700,900&display=swap"
        rel="stylesheet">
    <link rel="stylesheet" href="assets/css/style.min.css">
</head>

<body>
    <header>
        <?php
// Appel du header
require_once 'header.php';
?>
    </header>

    <section>
    <?php
// Vérification que la page demandée existe bien sinon redirection vers la page d'accueil
if (isset($_GET['page']) && file_exists($_GET['page'] . ".php")) {
    require_once $_GET['page'] . '.php';
} else {
    require_once 'home.php';
}
?>
    </section>

    <footer>
        <?php
// Appel du footer
require_once 'footer.php';
?>
    </footer>
    <a href="#"><i class="fas fa-chevron-circle-up"></i></a>

    <script
  src="https://code.jquery.com/jquery-3.4.1.min.js"
  integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
  crossorigin="anonymous"></script>
  <script src='https://kit.fontawesome.com/a076d05399.js'></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    <script src="assets/js/script.min.js"></script>

</body>

</html>