<?php
require_once("autoload.php");

// Suppression dynamique d'un produit du panier via une requête AJAX
if(isset($_POST["productId"])) {
    $user = new Users();
    $user = $_SESSION["user"];
    $cart = new Cart($user->getId());
    $cart->setProductId($_POST["productId"]);
    $cart->delete_cart();
    $result = $cart->total_amount();
    echo json_encode(array('data'=>$result));
}
?>